package Rubens;
import robocode.*;
//import java.awt.Color;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * Rubens - a robot by (Rubens Rafael)
 * O objetivo do robô é procurar por inimigos, e quando acertar algum, rodar em volta do inimigo para tentar desviar do disparos.
 */
public class Rubens extends Robot
{
	/**
	 * run: Rubens's default behavior
	 */
	public void run() {
		// Initialization of the robot should be put here

		// After trying out your robot, try uncommenting the import at the top,
		// and the next line:

		// setColors(Color.red,Color.blue,Color.green); // body,gun,radar

		// Robot main loop
		while(true) {
			// Replace the next 4 lines with any behavior you would like
			ahead(100);
			turnGunRight(180);
	
			
		}
	}

	/**
	 * onScannedRobot: What to do when you see another robot
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		// Replace the next line with any behavior you would like	
		double angulo = e.getBearing();
		double distancia = e.getDistance();
	
		if (distancia > 0) {
			turnGunRight(angulo); 
			turnRight(angulo);
			fire(2);
		}
	}


	/**
	 * onHitByBullet: What to do when you're hit by a bullet
	 */
	public void onHitByBullet(HitByBulletEvent e) {
		// Replace the next line with any behavior you would like
		back(10);
	}
	
	/**
	 * onHitWall: What to do when you hit a wall
	 */
	public void onHitWall(HitWallEvent e) {
		// Replace the next line with any behavior you would like
		turnLeft(180);
	}	
	
	public void onBulletHit(BulletHitEvent e){
		
		ahead(50);
		fire(5);
		fire(1);
		back(100);
		turnLeft(25);

		
		
		
	}
	}

