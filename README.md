Antes de começar eu queria avisar sobre o arquivo json. Eu não entendi muito sobre a questão dos diretórios, então deixei naquele padrão. Não fiz perguntas no post que tinha as instruções porqûe eu me inscrevi hoje e quando eu recebi o e-mail à noite, fui fazer o robô e resolvi explicar por aqui mesmo, para não perder tempo. Espero que entendam.

Mas iremos ao robô.

É um robô simples, consegui fazê-lo em algumas horas, o bjetivo dele é procurar um outro robô e e voltar em sua direção quando encontrar, e atacar. Se acertar o inimigo ele o o perseguirá e ficará rodando em volta dele, para desviar dos disparos que serão revidados.

Aprender sobre o robocode foi muito interesante, e o que eu mais gostei de foi sobre a estrutura de eventos e como o robô responde a cada um deles, seja coletando uma informação ou fazendo alguma ação.

Como ponto forte do meu robô é a movimentação, já que ele tem como objetivo "driblar" o oponente.
Como ponto fraco, tem o fato de os movimentos do loop básico interferirem na mira dele, depois de já ter identificado o alvo.

Estou muito contente em conseguir realizar esse desafio.
#Quero ser DEV!